package com.iu.service.impl.author;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iu.bean.author.Permission;
import com.iu.service.author.PermissionService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:spring-servlet.xml" })
public class PermissionServiceImplTest {

    @Autowired
    private PermissionService permissionService;
    
    @Test
    public void testCreatePermission() {
        Permission bean = new Permission( "user:ceate2", "des", true );
        bean = permissionService.createPermission( bean );
        System.out.println( bean.toString() );
        
    }

    @Test
    public void testDeletePermission() {
        Permission bean = new Permission( "user:ceate" + new Date().getTime(), "des", true );
        bean = permissionService.createPermission( bean );
        int count = permissionService.deletePermission( bean.getId() );
        System.out.println( "deletePermission-" + count );
    }

}
