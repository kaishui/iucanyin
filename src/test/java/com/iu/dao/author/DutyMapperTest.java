package com.iu.dao.author;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.iu.bean.author.Duty;
import com.iu.dao.author.DutyDao;
import com.iu.service.author.DutyService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:spring-servlet.xml" })
public class DutyMapperTest {

    @Autowired
    private DutyService dutyService;

    /**
     * 测试插入 作者: fengzt 创建日期:2014年5月29日
     */
    @Test
    public void insertDutyTest() {

      int count = dutyService.getDutyById();
        System.out.println( new Date() + "-----" + count );
        System.out.println("test");
    }

}
