<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<body>
<shiro:guest>
    欢迎游客访问，<a href="${pageContext.request.contextPath}/login.jsp">点击登录</a><br/>
</shiro:guest>
<shiro:user>
    欢迎[<shiro:principal/>]登录，<a href="${pageContext.request.contextPath}/duty/logout.do">点击退出</a><br/>
</shiro:user>
<shiro:hasAnyRoles name="userController">
   <shiro:principal/>拥有角色userController
</shiro:hasAnyRoles>

<shiro:hasAnyRoles name="admin">
   <shiro:principal/>拥有角色admin
</shiro:hasAnyRoles>

<shiro:hasPermission name="admin:create">
    <a href="createUser.jsp">拥有admin:create权限</a>
</shiro:hasPermission>

<shiro:hasPermission name="admin:update">
    <a href="createUser.jsp">拥有admin:update权限</a>
</shiro:hasPermission>
<shiro:hasPermission name="admin:*">
    <a href="createUser.jsp">拥有admin:*权限</a>
</shiro:hasPermission>
</body>
</html>
