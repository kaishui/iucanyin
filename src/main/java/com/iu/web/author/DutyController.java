package com.iu.web.author;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.iu.service.author.DutyService;
import com.iu.util.Tools;

/**
 * 
 * @title: 值别
 * @description: {desc}
 * @company: gdyd
 * @className: DutyController.java
 * @author: fengzt
 * @createDate: 2015年8月6日
 * @updateUser: fengzt
 * @version: 1.0
 */
@Controller
@RequestMapping("duty")
public class DutyController {
	private Logger log = Logger.getLogger(DutyController.class);
    
    @Autowired
    private DutyService dutyService;
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private HttpServletResponse response;

    @RequestMapping("/index")
    public ModelAndView index() throws IOException{


         return new ModelAndView("/login.jsp");
    }
    
    @RequestMapping("/weixinMunu")
    public @ResponseBody String weixinMunu() throws IOException{
    	 response.setCharacterEncoding("UTF-8");
         response.setContentType("text/xml");
         ServletInputStream in = request.getInputStream();
         String xmlMsg = Tools.inputStream2String(in);
         log.debug("输入消息:[" + xmlMsg + "]");
         //String xml = WeChat.processing(xmlMsg);
         
         String outMsg = "<xml>" +
        		 "<ToUserName><![CDATA[ofqq4wS-FD456uw-B_JGmPrhToTU]]></ToUserName>" +
        		 "<FromUserName><![CDATA[gh_a0070c108c03]]></FromUserName>" +
        		 "<CreateTime>" + new Date().getTime() + "</CreateTime>" +
        		 "<MsgType><![CDATA[text]]></MsgType>" +
        		 "<Content><![CDATA[ 你好 ！！ hello  ]]></Content>" +
        		 "</xml>";
         return outMsg;
    }
    
    @RequestMapping("/logout")
    public ModelAndView logout(){
    	HttpSession session = request.getSession();
    	String sessionId = session.getId();
    	dutyService.logout( sessionId );
    	
    	return new ModelAndView("/login");
    }
    
    @RequestMapping("/login")
    public ModelAndView login( String username, String password ){
    	//dutyService.login( username, password );
    	
    	Subject currentUser = SecurityUtils.getSubject();
    	System.out.println( (String)currentUser.getPrincipal() );
		UsernamePasswordToken token = new UsernamePasswordToken( username, password );
		try {
		    currentUser.login( token );
		} catch ( UnknownAccountException uae ) {
			System.out.println( uae.getMessage());
			throw( uae );
		} catch ( IncorrectCredentialsException ice ) {
			System.out.println( ice.getMessage());
			throw( ice );
		} catch ( LockedAccountException lae ) {
			System.out.println( lae.getMessage());
			throw( lae );
		}catch ( AuthenticationException ae ) {
			System.out.println( ae.getMessage());
			throw( ae );
		}
    	return new ModelAndView("/index");
    }
}
