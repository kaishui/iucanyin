package com.iu.service.impl.author;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iu.dao.author.DutyDao;
import com.iu.service.author.DutyService;

/**
 * @title: {title}
 * @description: {desc}
 * @company: iu
 * @className: DutyServiceImpl.java
 * @author: fengzt
 * @createDate: 2015年8月11日
 * @updateUser: fengzt
 * @version: 1.0
 */
@Service("dutyService")
public class DutyServiceImpl implements DutyService {

	@Autowired
	private DutyDao dutyDao;

	public int getDutyById() {
		return dutyDao.getDutyById();
	}

	@Override
	public void logout(String sessionId) {

		Subject currentUser = SecurityUtils.getSubject();
		currentUser.logout();
	}

	@Override
	public void login( String username, String password ) {
		Subject currentUser = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken( username, password );
		try {
		    currentUser.login( token );
		} catch ( UnknownAccountException uae ) {
			System.out.println( uae.getMessage());
		} catch ( IncorrectCredentialsException ice ) {
			System.out.println( ice.getMessage());
		} catch ( LockedAccountException lae ) {
			System.out.println( lae.getMessage());
		}catch ( AuthenticationException ae ) {
			System.out.println( ae.getMessage());
		}
	}

}
