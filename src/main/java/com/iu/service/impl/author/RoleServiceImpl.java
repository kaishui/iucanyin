package com.iu.service.impl.author;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.iu.bean.author.Role;
import com.iu.bean.author.RolePermssion;
import com.iu.dao.author.RoleDao;
import com.iu.service.author.RoleService;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
@Service
@Transactional(propagation=Propagation.SUPPORTS)
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;


    @Transactional(propagation=Propagation.REQUIRED)
    public Role createRole(Role role) {
         roleDao.createRole(role);
         return role;
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public int deleteRole(Long roleId) {
        return roleDao.deleteRole(roleId);
    }

    /**
     * 添加角色-权限之间关系
     * @param roleId
     * @param permissionIds
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public int correlationPermissions(Long roleId, Long... permissionIds) {
        List<RolePermssion> rolePermssions = new ArrayList<RolePermssion>();
        
        for( Long permissionId : permissionIds ){
            RolePermssion bean = new RolePermssion();
            bean.setPermissionId( permissionId );
            bean.setRoleId( roleId );
        }
        
        return roleDao.correlationPermissions(rolePermssions);
    }

    /**
     * 移除角色-权限之间关系
     * @param roleId
     * @param permissionIds
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public int uncorrelationPermissions(Long roleId, Long... permissionIds) {
        List<RolePermssion> rolePermssions = new ArrayList<RolePermssion>();
        
        for( Long permissionId : permissionIds ){
            RolePermssion bean = new RolePermssion();
            bean.setPermissionId( permissionId );
            bean.setRoleId( roleId );
        }
        
        return roleDao.uncorrelationPermissions(rolePermssions);
    }

}
