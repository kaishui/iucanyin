package com.iu.service.impl.author;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.iu.bean.author.Permission;
import com.iu.dao.author.PermissionDao;
import com.iu.service.author.PermissionService;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    

    @Autowired
    private PermissionDao permissionDao;

    @Transactional(propagation=Propagation.REQUIRED)
    public Permission createPermission(Permission permission) {
        int count =  permissionDao.createPermission(permission);
        System.out.println( count + " 更新条数 ！" );
        return permission;
    }

    public int deletePermission(Long permissionId) {
        return permissionDao.deletePermission(permissionId);
    }
}
