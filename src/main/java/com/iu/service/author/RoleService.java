package com.iu.service.author;

import com.iu.bean.author.Role;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public interface RoleService {


    public Role createRole(Role role);
    public int deleteRole(Long roleId);

    /**
     * 添加角色-权限之间关系
     * @param roleId
     * @param permissionIds
     * @return 
     */
    public int correlationPermissions(Long roleId, Long... permissionIds);

    /**
     * 移除角色-权限之间关系
     * @param roleId
     * @param permissionIds
     * @return 
     */
    public int uncorrelationPermissions(Long roleId, Long... permissionIds);

}
