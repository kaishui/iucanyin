package com.iu.service.author;


public interface DutyService {
    
    public int getDutyById( );

    /**
     * logout
     * @param sessionId 
     */
	public void logout(String sessionId);


	void login(String username, String password);

}
