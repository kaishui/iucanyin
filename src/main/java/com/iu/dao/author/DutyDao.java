package com.iu.dao.author;


/**
 * 
 * @title: DutyMapper 
 * @description: mybatis 接口
 * @company: gdyd
 * @className: DutyMapper.java
 * @author: fengzt
 * @createDate: 2014年6月4日
 * @updateUser: fengzt
 * @version: 1.0
 */
public interface DutyDao {
	
    /**
     * 
     * @description:查询结果中返回主键id的方法
     * @author: fengzt
     * @createDate: 2014年6月4日
     * @param duty  其中id为自增，不需要设置
     * @return 
     */
    public int getDutyById( );
}
