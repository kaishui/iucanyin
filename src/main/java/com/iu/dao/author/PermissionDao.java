package com.iu.dao.author;

import com.iu.bean.author.Permission;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public interface PermissionDao {

    public int createPermission(Permission permission);

    public int deletePermission(Long permissionId);

}
