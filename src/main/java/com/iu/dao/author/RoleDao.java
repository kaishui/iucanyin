package com.iu.dao.author;

import java.util.List;

import com.iu.bean.author.Role;
import com.iu.bean.author.RolePermssion;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public interface RoleDao {

    public int createRole(Role role);
    public int deleteRole(Long roleId);

    public int correlationPermissions(List<RolePermssion> list );
    public int uncorrelationPermissions(List<RolePermssion> list);

}
