package com.iu.dao.author;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.iu.bean.author.Permission;
import com.iu.bean.author.Role;
import com.iu.bean.author.User;
import com.iu.bean.author.UserRole;

/**
 * <p>User: Zhang Kaitao
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public interface UserDao {

    public int createUser(User user);
    public int updateUser(User user);
    public int deleteUser(Long userId);

    public int correlationRoles( List<UserRole> list );
    public int uncorrelationRoles(List<UserRole> list);
    
    public int exists(Map<Object, Object> map );

    User findOne(Long userId);

    User findByUsername(String username);

    List<Role> findRoles(String username);

    List<Permission> findPermissions(String username);
}
